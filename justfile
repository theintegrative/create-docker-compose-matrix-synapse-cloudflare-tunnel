# Default just chooser
default:
  @just --choose

# Copy configuration file
config:
  sudo cp homeserver.yaml data/homeserver.yaml

# Initialize configuration by using hostname
init hostname: config && up
  docker run -it --rm  -e SYNAPSE_SERVER_NAME={{ hostname }} -e SYNAPSE_REPORT_STATS=yes --mount type=volume,src=synapse-data,dst=/data matrixdotorg/synapse:latest migrate_config

# Bring docker compose up
up:
  docker compose up -d

# Bring docker compose down
down:
  docker compose down --remove-orphans

# Document all recipes copy to clipboard
docs:
 cat just -l | xclip -sel c
 $EDITOR README.md +/Available recipes:
