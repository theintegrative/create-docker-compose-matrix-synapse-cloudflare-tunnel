#  Create docker compose matrix synapse cloudflare tunnel
With this project you can easily create an matrix synapse server using cloudflare tunnel. 

## Dependencies
- [just](https://github.com/casey/just) # You can also run the commands seperately 
- [docker compose](https://github.com/docker/compose)

## Getting started
Clone this project:
``` shell
git clone git@gitlab.com/theintegrative/create-docker-compose-matrix-synapse-cloudflare-tunnel 
```
### Configure
Edit config `.env`:
``` shell
VIRTUAL_HOST="localhost"
VIRTUAL_PORT=8008
LETSENCRYPT_HOST=$VIRTUAL_HOST
SYNAPSE_SERVER_NAME=$VIRTUAL_HOST
SYNAPSE_REPORT_STATS="yes"
```

### Run
Run the jusfile:
``` shell
cd create-docker-compose-matrix-synapse-cloudflare-tunnel
# Change localhost for your server
just init localhost
``` 

## Recipes
``` shell
$ just -l
Available recipes:
    config        # Copy configuration file
    default       # Default just chooser
    docs          # Document all recipes copy to clipboard
    down          # Bring docker compose down
    exec          # Go into the docker container
    init hostname # Initialize configuration by using hostname
    up            # Bring docker compose up 
```
